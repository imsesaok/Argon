#!/usr/bin/env python3

import secrets
import requests
from argon.command import Command
from argon import string_util

# Servers may become inaccessible anytime.
# Check public SearX instances in https://github.com/asciimoo/searx/wiki/Searx-instances or https://stats.searx.xyz/.
# Servers also may not offer JSON result or block automated request. Check with test_servers.py.

searxes = ["https://trovu.komun.org/",       "https://searx.ca/",               "https://searx.canox.net/", 
           "https://searx.ch/",              "https://searx.drakonix.net/",     "https://wtf.roflcopter.fr/searx/",
           "https://searchx.mobi/",          "https://timdor.noip.me/searx/",   "https://srx.sx/",
           "https://ai.deafpray.wtf/searx/", "https://anonyk.com/",             "https://beezboo.com/",
           "https://burtrum.org/searx/",     "https://dynabyte.ca/",            "https://unmonito.red/",
           "https://framabee.org/",          "https://goso.ga/",                "https://haku.ahmia.fi/",
           "https://huyo.me/",               "https://intelme.com/",            "https://jsearch.pw/",
           "https://le-dahut.com/searx/",    "https://mijisou.com/",            "https://suche.uferwerk.org/",
           "https://openworlds.info/",       "https://perfectpixel.de/searx/",  "https://rapu.nz/",
           "https://roflcopter.fr/",         "https://s.cmd.gg/",               "https://search.activemail.de/", 
           "https://search.anonymize.com/",  "https://search.azkware.net/",     "https://search.datensturm.net/", 
           "https://search.ethibox.fr/",     "https://search.fossdaily.xyz/",  "https://seeks.hsbp.org/",
           "https://search.gibberfish.org/", "https://search.lgbtq.cool/",      "https://search.mdosch.de/", 
           "https://search.modalogi.com/",   "https://search.moravit.com/",     "https://search.paulla.asso.fr/", 
           "https://search.poal.co/",        "https://search.seds.nl/",         "https://search.snopyta.org/", 
           "https://search.spaeth.me/",      "https://search.st8.at/",          "https://search.stinpriza.org/", 
           "https://search.sudo-i.net/",     "https://search.tolstoevsky.ml/",  "https://searx.anongoth.pl/",
           "https://searx.cybt.de/",         "https://searx.dnswarden.com/",    "https://searx.li/",
           "https://searx.elukerio.org/",    "https://searx.everdot.org/",      "https://searx.foo.li/", 
           "https://searx.fr32k.de/",        "https://searx.good.one.pl/",      "https://searx.laquadrature.net/",
           "https://searx.gotrust.de/",      "https://searx.hardwired.link/",   "https://searx.itunix.eu/",
           "https://searx.libmail.eu/",      "https://searx.lynnesbian.space/", "https://spot.ecloud.global/",
           "https://searx.nakhan.net/",      "https://searx.nixnet.xyz/",       "https://searx.nnto.net/", 
           "https://searx.openhoofd.nl/",    "https://searx.openpandora.org/",  "https://searx.operationtulip.com/", 
           "https://searx.orcadian.net/",    "https://searx.pofilo.fr/",        "https://searx.prvcy.eu/", 
           "https://searx.pwoss.xyz/",       "https://searx.remote-shell.net/", "https://searx.ro/", 
           "https://searx.ru/",              "https://searx.site/",             "https://searx.solusar.de/", 
           "https://searx.targaryen.house/", "https://searx.tuxcloud.net/",     "https://searx.wegeeks.win/", 
           "https://searx.win/",             "https://searx.zareldyn.net/",     "https://searx.zdechov.net/",
           "https://suche.ftp.sh/"]


def request_searx(query, category, prev_servers=[]):
    server = secrets.choice(list(set(searxes)-set(prev_servers)))
    params = {'q': query, 'categories': category, 'safesearch': '2',
              'format': 'json', 'image_proxy': "True", 'language': 'en'}
    try:
        response = requests.get("{}?".format(
            server if server[-1] == '/' else server+"/"), params=params, headers={'User-agent': 'ArgonBot 2.0'})
        return server, response.json()
    except:
        print(server+" has not provided the results")
        if len(prev_servers) > 10:
            return None, None
        prev_servers.append(server)
        return request_searx(query, category, prev_servers)


def to_list(from_list, infix):
    string = ""
    size = len(from_list)
    for i in range(size):
        if i+1 is size and i > 0:
            string += infix+" "
        string += "'{}'".format(from_list[i])
        if i+1 is not size:
            string += ", "
    return string


def query(match, metadata, bot):
    if not match.group("query"):
        return
    reply = ""

    for _ in range(4):
        server, result_json = request_searx(match.group("query"), "general")
        if result_json and 'infoboxes' in result_json.keys() and len(result_json['infoboxes']):
            result = result_json['infoboxes'][0]

            if 'content' in result.keys() and result['content']:
                result['content'] = string_util.truncated_by_sentence(
                    result['content'])
                reply += '{content}\n'.format(**result)
            for link in result['urls']:
                reply += '{title}: {url} '.format(**link)
            break
        elif not server:
            reply = "The query has been rejected in too many servers; try again later."
            break

    if not reply:
        reply = "No results for '{}'".format(match.group("query"))

    bot.reply(reply, metadata["message_id"],
              metadata['from_group'], metadata['_id'])


def register_with(argon):
    argon.add_commands(
        # Instant Answers
        Command(r"\?(?: (?P<query>.+))?", "? <query>",
                "Search for Instant Answer in SearX instances", query),
    )
