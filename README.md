# Argon

![](docs/argon.png)

Multi-platform chatbot in Python.

⚠️ This project is of alpha quality. The structure and plugin API can change dramatically.

This project aims to support many IM platform as possible. Currently, adapters are developed for IRC, the Telegram Bot API\*, and [Matrix](https://matrix.org/).

(\*Proprietary platform)

License: AGPL 3.0+

## Installing for development

Requirements:
* Python ≥3.5, currently developed against 3.7 and also tested against 3.5
* [Pipenv](https://pypi.org/project/pipenv/)

1. Install dependencies with `pipenv install`.
2. Run with `pipenv run ./argonbot.py --console` to run the console adapter.

## Deployment

Requirements:
* Python ≥3.5, currently developed against 3.7 and also tested against 3.5
* [Pipenv](https://pypi.org/project/pipenv/)

1. Install dependencies with `pipenv install`.
2. Set environment variables. Look in 
3. Run with `pipenv run ./argonbot.py --console` to run the console adapter.

This program is runnable on Heroku.

### Configuration

Currently configuration can be only done through environment variables.

* `SOURCE_URL`: URL where the source code for your bot lives. If you're just deploying our
  version, set this to the URL of the official repo. Note that our [license (AGPL)](LICENSE)
  obliges you to publish your modifications if you deploy the software!
* `PRIVACY_POLICY_URL`: URL where users can find the privacy policy of your bot.

#### Telegram
* `ARGON_TELEGRAM`: set this to anything non-empty to enable the Telegram adapter.
* `TELEGRAM_BOT_TOKEN`
* `TELEGRAM_BOT_OWNER`
* `TELEGRAM_BOT_OWNER_ID`
* `TELEGRAM_IDENTIFIER`: prefix for commands.

#### IRC
* `ARGON_IRC`: set this to anything non-empty to enable the IRC adapter.
* `IRC_SERVER_ADDRESS` and `IRC_SERVER_PORT`: address and port of the IRC server. Currently only
  one server is supported but it's pretty easy to hardcode more in [`argonbot.py`](argonbot.py)
  if you want.
* `IRC_SERVER_IS_SSL`: whether to connect over an encrypted connection (recommended if the IRC
  network supports it)
* `IRC_CHANNELS`: comma-separated list of channels to join. Example: `#argonbot,##argonbot-bottest`
* `IRC_OWNER`: nick of the owner of the bot. Currently being a bot's owner doesn't allow you to do
  much, but this will change later.
* `IRC_NICK`: nick that the bot should take.
* `IRC_SERVER_IS_SASL`: set this to anything non-empty if you have registered an identity
  for the bot and want to authenticate, using SASL (recommended). (Documentation about registration
  on Freenode: https://freenode.net/kb/answer/registration)
* `IRC_SASL_PASSWORD`: password to use while authenticating over SASL. The used username is the
  nick.
* `IRC_IDENTIFIER`: prefix for commands.

#### Matrix
* `ARGON_MATRIX`: set this to anything non-empty to enable the Matrix adapter.
* `MATRIX_SERVER`
* `MATRIX_USERNAME`
* `MATRIX_OWNER`
* `MATRIX_DEVICE_ID`
* `MATRIX_IDENTIFIER`: prefix for commands.
