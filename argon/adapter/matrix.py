from datetime import datetime
import logging
import re
import traceback
import threading
from argon.adapter import Adapter


class MatrixAdapter(Adapter):
    def __init__(self, server, username, password, owner, device_id=None, rooms=None, identifier="!"):
        super(MatrixAdapter, self).__init__(identifier=identifier)
        self.server = server
        self.username = username
        self.password = password
        self.owner = owner
        self.rooms = rooms if bool(rooms) else []
        self.device_id = device_id
        self.handled_rooms = {}

        self.logger = logging.getLogger("MatrixAdapter")
        threading.Thread.__init__(self)
        logging.basicConfig(level=logging.INFO)

    def find_element_in_list(self, element, list_element):
        try:
            index_element = list_element.index(element)
            return index_element
        except ValueError:
            return None

    def display_name_for_room(self, room, user_id):
        for member in room._members:
            if member.user_id == user_id:
                return member.get_display_name()
        else:
            return None

    def register_callback(self, func, _id):
        self.execute = func
        self._id = _id

    def send(self, message, room):
        # todo: markdownify?
        self.logger.info("To room ``{}'': ``{}''".format(room, message))
        # room.send_text(message)

    def reply(self, message, to, room):
        # todo: handle apiaceae and pals
        formatted_message = '<a href="https://matrix.to/#/{}">{}</a>: {}'.format(
            to, self.display_name_for_room(room, to), message)
        plain_text_message = '{}: {}'.format(
            self.display_name_for_room(room, to), message)
        self.logger.info("To ``{}'' in room ``{}'': ``{}''".format(
            to, room. room_id, formatted_message))
        room.send_html(formatted_message, body=plain_text_message)

    def eval(self, room, event):
        # Ignore our own events
        if event['sender'] == self.client.user_id:
            return
        # Force only mentions
        if self.display_name_for_room(room.room_id, self.client.user_id) not in event['content']['body']:
            return

        user = event['sender']
        message = event['content']['body']
        origin_server_ts = datetime.fromtimestamp(
            int(event['origin_server_ts'])/1000)

        # Check if the message was sent through a bridging bot

        # First we check the sender's nick
        # Remember that event['sender'] is the Matrix id,
        # @<network>_<name>:<domain>
        m_user = re.fullmatch(
            r'@(?:.+_)?(apiaceae|xxxx_|^^^^):.+', event['sender'])
        if m_user:
            # Then we check the message.
            # Matrix escapes IRC color codes to plain text
            m_message = re.fullmatch(
                r'<(?:\x03[^\x02]*\x02*)?([^>\x03]+)\x03?> *: (.+)', event['content']['body'])

            if m_message:
                # Move information so that we think this message came from that user
                # Append [proxy] to avoid impersonation
                proxy_user = m_user[1]
                user = "{}[{}]".format(m_message[1], proxy_user)
                message = m_message[2]

        self.logger.info("Received message in ``{}'' from ``{}'': ``{}''".format(
            room.display_name, user, message))

        # Since Matrix has no concept of private rooms, make them optional
        # and force mention check
        ident = self.identifier_optional

        # Bot reply signature: message, to, room, adapter ID
        metadata = {"from_user": user,
                    "from_group": room,
                    "when": origin_server_ts,
                    "_id": self._id,
                    "ident": ident,
                    "type": self.__class__.__name__,
                    "mentioned": True,
                    # FIXME: Check for other admins in the channel.
                    "is_mod": user == self.owner,
                    "message_id": user}
        self.execute(message, metadata)

    def handle_invite(self, room_id, state):
        self.logger.info("Got invite to room: {}, joining".format(room_id))
        room = self.client.join_room(room_id)
        self.logger.info(
            "Listening to events from {}".format(room.display_name))
        room.add_listener(self.eval, event_type="m.room.message")
        self.handled_rooms[room_id] = room

    def run(self):
        from matrix_client.client import MatrixClient
        from matrix_client.api import MatrixRequestError

        self.client = MatrixClient(self.server)
        try:
            self.logger.info("Logging in...")
            self.client.login(self.username, self.password,
                              device_id=self.device_id)
        except MatrixRequestError as e:
            if e.code == 403:
                self.logger.error("Bad username/password")
            else:
                self.logger.error(traceback.format_exc())
            return
        except Exception:
            self.logger.error(traceback.format_exc())
            return

        self.logger.info("Logged in succesfully!")

        self.client.api.update_device_info(self.device_id, "StarBot")

        self.client.add_invite_listener(self.handle_invite)

        for room_id, room in self.client.get_rooms().items():
            self.logger.info(
                "Listening to events from {}".format(room.display_name))
            room.add_listener(self.eval, event_type="m.room.message")
            self.handled_rooms[room_id] = room

        for room_id in self.rooms:
            if self.handled_rooms[room_id]:
                continue
            room = self.client.join_room(room_id)
            self.logger.info(
                "Listening to events from {}".format(room.display_name))
            room.add_listener(self.eval, event_type="m.room.message")
            self.handled_rooms[room_id] = room

        self.client.start_listener_thread()

        self.client.sync_thread.join()

        # self._on_initialized(self.owner)
