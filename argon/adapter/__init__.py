import threading
from datetime import datetime
from argon.constants import VERSION


class Adapter(threading.Thread):
    def __init__(self, identifier="!"):
        self.identifier = identifier
        self.identifier_optional = "(?:" + self.identifier + ")?"
        self.owner = None
        self.startup_errors = []

    def _on_initialized(self, to):
        self.send("I'm awake! Version {}".format(VERSION), to)
        self.send("Startup time: " + datetime.now().isoformat(), to)
        # Report startup errors to owner
        if self.startup_errors:
            self.send("Startup errors:", to)
        else:
            self.send("All things normal!", to)
        for err in self.startup_errors:
            self.send("• {}".format(err), to)

    def register_callback(self, func, _id):
        pass

    def send(self, message, group):
        pass

    def reply(self, message, to, group):
        pass
