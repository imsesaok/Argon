#!/usr/bin/env python3

import sys
import traceback
from os import path, listdir
from importlib.util import spec_from_file_location
import argon as ar


BASE_DIR = path.join(path.dirname(path.realpath(__file__)), "..")
DEFAULT_DIR = "plugins"


class FailedPlugin:
    def __init__(self, name=""):
        self.ARGON_MODULE_NAME = "failed plugin " + name

    def register_with(*args, **kwargs):
        pass


def strip_extension(filename: str) -> str:
    return filename[:filename.rindex(".")]


def _notify_failure(msg, argon):
    print(msg, file=sys.stderr, end=" ")
    traceback.print_exc(file=sys.stderr)
    for adapter in argon.adapters.values():
        adapter.startup_errors.append(msg)


def _load_module(name, path, argon: "ar.Argon"):
    # Get a module loading object
    try:
        loader = spec_from_file_location(name, path).loader

    except:
        _notify_failure("Failed to prepare module `{}` for loading.".format(name), argon)
        return FailedPlugin(name)

    # Actually load the module
    try:
        module = loader.load_module()

    except:
        _notify_failure("Failed to load module `{}`.".format(name), argon)
        return FailedPlugin(name)

    module.ARGON_MODULE_NAME = name

    # Return the module
    return module


def load_all(argon: "ar.Argon", directory=DEFAULT_DIR) -> dict:
    if not path.isabs(directory):
        directory = path.join(BASE_DIR, directory)

    f = lambda filename: path.join(directory, filename)

    return {
        strip_extension(filename):
            _load_module(
                name=strip_extension(filename),
                path=f(filename),
                argon=argon
            )
            for filename in listdir(directory)
            if path.isfile(f(filename)) and filename.endswith(".py")
    }


def register_all(argon: "ar.Argon", plugins) -> None:
    for plugin in plugins:
        name = "couldn't detect name, this is probably not a module loaded with plugin_loader"
        try:
            name = plugin.ARGON_MODULE_NAME
            plugin.register_with(argon)

        except:
            _notify_failure("Failed to register module `{}`.".format(name), argon)
            continue

        print("Plugin `{}` registered".format(name))


def load_and_register_all(argon: "ar.Argon", directory=DEFAULT_DIR) -> list:
    plugins = load_all(argon, directory)
    register_all(argon, plugins.values())
    return plugins

